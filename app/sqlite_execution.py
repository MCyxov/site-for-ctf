import sqlite3


def get_user_permission(username):
    conn = sqlite3.connect("users.db")
    cursor = conn.cursor()
    cursor.execute("SELECT permission from users where username=?", [username])
    perm = cursor.fetchall()[0][0]
    conn.close()
    return perm


def get_user_score(username):
    if username == '':
        return
    conn = sqlite3.connect("users.db")
    cursor = conn.cursor()
    cursor.execute("SELECT point from users where username=?", [username])
    perm = cursor.fetchall()[0][0]
    conn.close()
    return perm


def get_all_tasks(username):
    conn = sqlite3.connect("users.db")
    cursor = conn.cursor()
    cursor.execute("SELECT solved from users where username=?", [username])
    solved = cursor.fetchall()[0][0].strip(':')
    conn.close()

    conn = sqlite3.connect("tasks.db")
    cursor = conn.cursor()
    cursor.execute("SELECT id, type, title, description, point FROM tasks")
    tasks = cursor.fetchall()
    conn.close()
    return tasks, solved


def get_task_from_id(task_id):
    conn = sqlite3.connect("tasks.db")
    cursor = conn.cursor()
    cursor.execute("SELECT * from tasks where id=?", [int(task_id)])
    task = cursor.fetchall()[0]
    conn.close()
    return task


def solved_task_update(task_id, username):
    conn = sqlite3.connect("tasks.db")
    cursor = conn.cursor()
    cursor.execute("SELECT point FROM tasks WHERE id=?", [str(task_id)])
    task_point = int(cursor.fetchall()[0][0])
    conn.close()

    conn = sqlite3.connect("users.db")
    cursor = conn.cursor()
    cursor.execute("SELECT solved FROM users WHERE username=?", [str(username)])
    solved = cursor.fetchall()[0][0] + str(task_id) + ':'
    cursor.execute("UPDATE users SET solved=? where username=?", [solved, username])
    cursor.execute("UPDATE users SET point=point+? where username=?", [int(get_user_score(username))+task_point, username])
    cursor.fetchall()
    conn.commit()
    conn.close()


def get_password_from_username(password):
    conn = sqlite3.connect("users.db")
    cursor = conn.cursor()
    cursor.execute("SELECT password FROM users WHERE username=?", [str(password)])
    ans = cursor.fetchall()
    conn.close()
    return ans


def get_user_solved_task_from_username(username):
    conn = sqlite3.connect("users.db")
    cursor = conn.cursor()
    cursor.execute("SELECT solved FROM users where username=?", [str(username)])
    solved = cursor.fetchall()
    conn.close()
    return solved


def add_new_user(username, password):
    conn = sqlite3.connect("users.db")
    cursor = conn.cursor()
    cursor.execute("INSERT INTO users(username, password, solved, permission) VALUES (?, ?, ?, ?)",
                   [str(username), str(password), '', 1])
    cursor.fetchall()
    conn.commit()
    conn.close()


def add_new_task(type_of_task, title, description, file, point, flag):
    conn = sqlite3.connect("tasks.db")
    cursor = conn.cursor()
    cursor.execute("INSERT INTO tasks(type, title, description, file, point, flag) VALUES "
                   "(?, ?, ?, ?, ?, ?)", [str(type_of_task), str(title), str(description), str(file), str(point), str(flag)])
    cursor.fetchall()
    conn.commit()
    conn.close()


def get_all_users():
    conn = sqlite3.connect("users.db")
    cursor = conn.cursor()
    cursor.execute("SELECT point, username, id FROM users")
    users = cursor.fetchall()
    conn.close()
    return users


def get_username_from_id(person_id):
    conn = sqlite3.connect("users.db")
    cursor = conn.cursor()
    cursor.execute("SELECT username from users where id=?", [person_id])
    username = cursor.fetchall()[0][0]
    conn.close()
    return username
