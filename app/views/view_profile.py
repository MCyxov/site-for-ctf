from flask import render_template, session
from app import app
from app.sqlite_execution import get_user_permission, get_user_score, \
    get_user_solved_task_from_username, get_username_from_id, get_task_from_id


@app.route('/profile/<int:profile_id>', methods=['GET', 'POST'])
def view_profile(profile_id):
    username = ''
    index_page = "index.html"
    if 'username' in session:
        username = session['username']
        if get_user_permission(session['username']) == 99:
            index_page = "index_admin.html"
    else:
        index_page = 'base.html'

    name = get_username_from_id(profile_id)
    solved = get_user_solved_task_from_username(name)[0][0].strip(':')
    tasks = [get_task_from_id(i) for i in solved]

    return render_template('view_profile.html',
                           username=username,
                           index=index_page,
                           score=get_user_score(username),
                           solved=tasks,
                           name=name)
