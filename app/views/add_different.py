import os

from flask import render_template, redirect, request, session
from werkzeug.utils import secure_filename

from app import app
from app.forms import Register, AddTask
from app.sqlite_execution import get_user_permission, get_user_solved_task_from_username, \
    add_new_user, add_new_task, get_user_score
import hashlib


@app.route('/add/user', methods=['GET', 'POST'])
def add_user():
    if not('username' in session) or session['username'] != 'admin':
        return redirect('/index')
    form = Register()
    error = ''
    if request.method == 'POST':
        name = str(request.form['name'])
        password = hashlib.md5(str(request.form['password']).encode('utf-8')).hexdigest()
        password_again = hashlib.md5(str(request.form['password_again']).encode('utf-8')).hexdigest()

        if password != password_again:
            error = "Your password must be equal!"

        res = get_user_solved_task_from_username(name)

        if len(res) > 0:
            error = "Username already exist!"

        if error == '':
            add_new_user(name, password)
            return redirect("/index")

    perm = get_user_permission(session['username'])
    index_page = "index.html"
    if perm == 99:
        index_page = "index_admin.html"

    return render_template('add_user.html',
                           title='New user',
                           form=form,
                           error=error,
                           index=index_page,
                           username=session['username'],
                           score=get_user_score(session['username']))


@app.route('/add/task', methods=['GET', 'POST'])
def add_task():
    if not('username' in session) or session['username'] != 'admin':
        return redirect('/index')
    form = AddTask()
    error = ''
    if request.method == 'POST':
        type_of_task = str(request.form['type'])
        title = str(request.form['title'])
        description = str(request.form['description'])
        file = request.files['file']
        point = str(request.form['point'])
        flag = str(request.form['flag'])

        if file:
            filename = secure_filename(file.filename)
            file_path = '../../static/task_file/' + filename
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        else:
            file_path = 0

        add_new_task(type_of_task, title, description, file_path, point, flag)
        return redirect("/index")

    perm = get_user_permission(session['username'])
    index_page = "index.html"
    if perm == 99:
        index_page = "index_admin.html"

    return render_template('add_task.html',
                           title='New user',
                           form=form,
                           password_error=error,
                           index=index_page,
                           username=session['username'],
                           score=get_user_score(session['username']))
