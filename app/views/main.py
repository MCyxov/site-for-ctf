from flask import render_template, redirect, session, request
from app import app
from app.forms import SubmitFlag
from app.sqlite_execution import get_user_permission, \
    get_all_tasks, get_task_from_id, solved_task_update, get_user_score, get_all_users


@app.errorhandler(404)
def not_found(e):
    return redirect('/index')


@app.route('/task/<int:task_id>', methods=['GET', 'POST'])
def show_task(task_id):
    if not ('username' in session):
        return redirect('/login')

    form = SubmitFlag()
    message = ''

    task = get_task_from_id(task_id)

    if request.method == 'POST':
        flag = str(request.form['flag'])
        if flag == task[6]:
            message = True
            solved_task_update(task_id, session['username'])
        else:
            message = False

    index_page = "index.html"
    if get_user_permission(session['username']) == 99:
        index_page = "index_admin.html"
    return render_template('show_task.html',
                           username=session['username'],
                           index=index_page,
                           type=task[0],
                           title=task[2],
                           description=task[3],
                           dop=task[4],
                           form=form,
                           message=message,
                           score=get_user_score(session['username']))


@app.route('/scoreboard')
def scoreboard():
    def compare(item1):
        return -1 * item1[0]

    index_page = "base.html"
    username = ''
    if 'username' in session:
        username = session['username']
        index_page = "index.html"
        perm = get_user_permission(username)
        if perm == 99:
            index_page = "index_admin.html"
    users = get_all_users()
    users.sort(key=compare)
    users = [[users[i], True if str(users[i][1]) == username else False, i+1] for i in range(len(users))]
    return render_template('scoreboard.html',
                           index=index_page,
                           username=username,
                           users=users,
                           score=get_user_score(username))


@app.route('/')
@app.route('/index')
def index():
    def cmp(item):
        if sort_kind == 'point':
            return item[4]
        elif sort_kind == 'title':
            return item[2]
        elif sort_kind == 'description':
            return item[3]
        else:
            return item[1]

    if not ('username' in session):
        return redirect('/login')

    index_page = "index.html"
    perm = get_user_permission(session['username'])
    sort_kind = request.args.get('sort')
    if sort_kind == '' or sort_kind is None:
        sort_kind = 'type'
    if perm == 99:
        index_page = "index_admin.html"
    elif not perm:
        return redirect('/login')
    tasks, solved = get_all_tasks(session['username'])
    tasks.sort(key=cmp)
    return render_template(index_page,
                           username=session['username'],
                           tasks=[[tasks[i], True if str(tasks[i][0]) in solved else False] for i in range(len(tasks))],
                           score=get_user_score(session['username']))
