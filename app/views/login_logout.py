from flask import render_template, redirect, request, session
from app import app
from app.forms import LoginForm
from app.sqlite_execution import get_password_from_username
import hashlib


@app.route('/login', methods=['GET', 'POST'])
def login():
    if 'username' in session:
        return redirect('/')

    form = LoginForm()
    if request.method == 'POST':
        name = str(request.form['name'])
        test_password = hashlib.md5(str(request.form['password']).encode('utf-8')).hexdigest()

        password = get_password_from_username(name)

        if (len(password) != 0 and password == test_password) or len(password) == 0:
            return render_template('login.html',
                                   title='Sign In',
                                   form=form,
                                   error="Wrong username or password")
        session['username'] = name
        return redirect('/index')

    return render_template('login.html',
                           title='Sign In',
                           form=form)


@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect('/login')
