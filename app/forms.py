from wtforms import PasswordField, BooleanField, Form, StringField, FileField, TextAreaField
from wtforms.validators import DataRequired, InputRequired


class LoginForm(Form):
    name = StringField('Name:', validators=[InputRequired()])
    password = PasswordField('Password:', [InputRequired()])


class Register(Form):
    name = StringField('Name:', validators=[InputRequired()])
    password = PasswordField('Password:', [InputRequired()])
    password_again = PasswordField('Password again:', [InputRequired()])


class AddTask(Form):
    type = StringField('Type:', validators=[InputRequired()])
    title = StringField('Title:', [InputRequired()])
    description = TextAreaField('Description:', [InputRequired()])
    file = FileField('Recipe Image', validators=[DataRequired()])
    point = StringField('Point:', [InputRequired()])
    flag = StringField('Flag:', [InputRequired()])


class SubmitFlag(Form):
    flag = PasswordField('Flag:', validators=[InputRequired()])
